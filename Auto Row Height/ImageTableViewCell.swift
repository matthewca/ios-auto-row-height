//
//  ImageTableViewCell.swift
//  Auto Row Height
//
//  Created by Matthew Smith on 2015-06-26.
//  Copyright (c) 2015 Matthew Smith. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {

    @IBOutlet weak var tableImage: UIImageView!
    
    func configureCellWith(image: String) {
        tableImage.image = UIImage(named: image)
    }

}
