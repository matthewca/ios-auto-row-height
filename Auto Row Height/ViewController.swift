//
//  ViewController.swift
//  Auto Row Height
//
//  Created by Matthew Smith on 2015-06-22.
//  Copyright (c) 2015 Matthew Smith. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    
    var images = ["abstract","city","city2","nightlife"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ImageTableViewCell
        cell.configureCellWith(images[indexPath.item])
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    
}

